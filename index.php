<?php

$folders = [
    'services',
    './',
];
$extension = 'php';
foreach ($folders as $folder) {
    spl_autoload_register(function ($class) use ($folder, $extension) {
        $path = __DIR__ . DIRECTORY_SEPARATOR . $folder . DIRECTORY_SEPARATOR . $class . '.' . $extension;
        if (file_exists($path)) {
            require $path;
        }
    });
}

/**
 * Возвращает массив с количеством голов
 * @param  int   $c1 сколько первая команда забила второй
 * @param  int   $c2 сколько вторая забила первой
 * @return array
 */
function match($c1, $c2){
    return MatchService::resultBetween($c1, $c2);
}
// var_dump(match(1, 9));