<?php

/**
 * Данный предсказатель счета ориентируется только на показатели побед
 * и поражений, другие данные не используются. Я подумал что, во-первых,
 * влияние количества ничьих на силу команды тяжело оценить, а во-вторых,
 * забитые голы также очень уж приблизительно дают понимание картины, потому
 * что нет данных кто и кому именно забивал, поэтому я решил игнорировать
 * эти числа. А вот победы и поражения - существенный признак силы команды.
 * Много голов могут наколотить и слабые команды, и это не сделает их
 * сильнее.
 */
class Matcher
{
    /**
     * Вероятность которая позволяет даже самой слабой команде
     * забить самой сильной команде, самая сильная команда
     * не может выигрывать у самой слабой со 100% вероятностью.
     * Этот шанс тем меньше, чем выше сила команды.
     */
    const BASE_CHANCE = 0.1;

    protected $c1;

    protected $c2;

    /**
     * Копия данных о командах
     * @var array
     */
    protected $teams;

    /**
     * Финальный счет
     * @var array
     */
    protected $finalResult;

    public function __construct($c1, $c2, $teams)
    {
        $this->c1 = $c1;
        $this->c2 = $c2;
        $this->teams = $teams;
        $this->finalResult = [0, 0];
    }

    public function do()
    {
        $this->calculateAbsoluteWinRate();
        $this->calculateAbsoluteLoseRate();
        $this->calculateAbsolutePowerRate();
        $this->predictMatchResults();
    }

    public function result()
    {
        return $this->finalResult;
    }

    protected function calculateAbsoluteWinRate()
    {
        $minWinRate = $this->teams[0]['win'];
        $maxWinRate = $this->teams[0]['win'];
        foreach ($this->teams as $team) {
            // сначала найдем минимальное и максимальное значение по победам
            $minWinRate = $team['win'] < $minWinRate ? $team['win'] : $minWinRate;
            $maxWinRate = $team['win'] > $maxWinRate ? $team['win'] : $maxWinRate;
        }
        foreach ($this->teams as &$team) {
            // затем переведем все количества побед в коэффициенты от 0.0(min) до 1.0(max)
            $teamValue = $team['win'] - $minWinRate;
            $minMaxRange = $maxWinRate - $minWinRate;
            if ($teamValue <= 0.0) {
                $team['absoluteWinRate'] = 0.0;
            } else {
                $team['absoluteWinRate'] = $teamValue / $minMaxRange;
            }
        }
    }

    protected function calculateAbsoluteLoseRate()
    {
        $minLoseRate = $this->teams[0]['defeat'];
        $maxLoseRate = $this->teams[0]['defeat'];
        foreach ($this->teams as $team) {
            // то же что и с победами, только поражения
            $minLoseRate = $team['defeat'] < $minLoseRate ? $team['defeat'] : $minLoseRate;
            $maxLoseRate = $team['defeat'] > $maxLoseRate ? $team['defeat'] : $maxLoseRate;
        }
        foreach ($this->teams as &$team) {
            // коэффициенты поражений
            $teamValue = $team['defeat'] - $minLoseRate;
            $minMaxRange = $maxLoseRate - $minLoseRate;
            if ($teamValue <= 0.0) {
                $team['absoluteLoseRate'] = 0.0;
            } else {
                $team['absoluteLoseRate'] = $teamValue / $minMaxRange;
            }
        }
    }

    protected function calculateAbsolutePowerRate()
    {
        $minPowerRate = $this->teams[0]['absoluteWinRate'] / $this->teams[0]['absoluteLoseRate'];
        $maxPowerRate = $this->teams[0]['absoluteWinRate'] / $this->teams[0]['absoluteLoseRate'];
        foreach ($this->teams as &$team) {
            // теперь находим относительный коэффициент силы команды,
            // просто поделив коэффициент побед на коэффициент поражений
            if ($team['absoluteLoseRate'] <= 0.0) {
                $team['relativePowerRate'] =  $team['absoluteWinRate'];
            } else {
                $team['relativePowerRate'] =  $team['absoluteWinRate'] / $team['absoluteLoseRate'];
            }
            $minPowerRate = min($team['relativePowerRate'], $minPowerRate);
            $maxPowerRate = max($team['relativePowerRate'], $maxPowerRate);
        }
        foreach ($this->teams as &$team) {
            // превратим относительный коэффициент в абсолютный, преобразовав его точно также,
            // как и победы - от 0.0 до 1.0
            $teamValue = $team['relativePowerRate'] - $minPowerRate;
            $minMaxRange = $maxPowerRate - $minPowerRate;
            if ($teamValue <= 0.0) {
                $team['absolutePowerRate'] = 0.0;
            } else {
                $team['absolutePowerRate'] = $teamValue / $minMaxRange;
            }
        }
    }

    protected function predictMatchResults()
    {
        if ($this->teams[$this->c1]['absolutePowerRate'] < 0.5) {
            // здесь применим постоянный коэффициент к текущему коэффициенту силы команды, -
            // это нужно что бы самая слабая команда с 0.0 силы имела хоть какой то шанс забить
            // команде с 1.0 силы (в реальности это возможно, сборная России в теории имеет
            // шанс забить Бразилии (и даже забивала? я не помню, не силен в футболе)).
            // шанс составляет 10% если команда имеет 0.0 силы, и уме почти 0% если сила приближается
            // к 0.5.
            // сильная команда также ослабляется в зависимости от своей силы, на позиции 1.0 будет отниматься 10% силы
            // (силы нужна для шанса забить)
            $modifiedRateC1 = $this->modifyAbsolutePowerRate('+', $this->c1);
        } else {
            $modifiedRateC1 = $this->modifyAbsolutePowerRate('-', $this->c1);
        }
        if ($this->teams[$this->c2]['absolutePowerRate'] < 0.5) {
            $modifiedRateC2 = $this->modifyAbsolutePowerRate('+', $this->c2);
        } else {
            $modifiedRateC2 = $this->modifyAbsolutePowerRate('-', $this->c2);
        }
        // предсказываем голы c1
        $c1GoalsCount = 0;
        // так как количество голов редко бывает выше 5, то возьмем 5 за максимум.
        // делается 5 попыток забить, рандомится float от 0.0 до 1.0, и сравнивается
        // с силой команды. 
        // если рандом меньше силы команды, то считаем что команда забила.
        // зрители спросят, а почему мы просто сравниваем с силой, вместо того что бы
        // сравнить силы двух команд, а затем с помощью рандома определить кто сколько
        // вколотил? 
        // все очень просто - дело в том, что мы уже приводили показатели команд к абсолютным
        // величинам, что уже является выражением отношения сил всех команд относительно друг друга.
        // текущий показатель силы и назван абсолютным потому, что он уже является показателем силы
        // в рамках данного набора данных. 
        foreach (range(1, 5) as $goalRate) {
            $goalChance = mt_rand(0, mt_getrandmax() - 1) / mt_getrandmax();
            $c1GoalsCount += ($goalChance < $modifiedRateC1) ? 1 : 0;
        }
        $this->finalResult[0] = $c1GoalsCount;
        // предсказываем голы c2
        $c2GoalsCount = 0;
        foreach (range(1, 5) as $goalRate) {
            $goalChance = mt_rand(0, mt_getrandmax() - 1) / mt_getrandmax();
            $c2GoalsCount += ($goalChance < $modifiedRateC2) ? 1 : 0;
        }
        $this->finalResult[1] = $c2GoalsCount;
    }

    protected function modifyAbsolutePowerRate($action, $teamIndex)
    {
        if ($action === '+') {
            if ($this->teams[$teamIndex]['absolutePowerRate'] !== 0.0) {
                return $this->teams[$teamIndex]['absolutePowerRate'] + (self::BASE_CHANCE - (self::BASE_CHANCE / (0.5 / $this->teams[$teamIndex]['absolutePowerRate'])));
            } else {
                return $this->teams[$teamIndex]['absolutePowerRate'] + self::BASE_CHANCE;
            }
        } elseif ($action === '-') {
            return $this->teams[$teamIndex]['absolutePowerRate'] - abs(self::BASE_CHANCE - (self::BASE_CHANCE / (0.5 / $this->teams[$teamIndex]['absolutePowerRate'])));
        } else {
            return $this->teams[$teamIndex]['absolutePowerRate'];
        }
    }
}